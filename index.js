require("dotenv").config();
const schedule = require("node-schedule");
const publicIp = require("public-ip");
const moment = require("moment-timezone");
const path = require("path");
const fs = require("fs");
const sleep = require("util").promisify(setTimeout);
const {
  schedulingRef,
  activeSpeakerRef,
  recordingsRef,
  db,
  bucket,
} = require("./util/admin");
const { logger } = require("./logger/logger");
const {
  getContainerByName,
  pushDockerList,
  pushDockerCon,
  pushRemoveDockerCon,
  pushDockerTranscode,
} = require("./docker/dockerApi");
const { buildDockerContainerName, getDetailsFromScheduleKey } = require('./util/naming');

let isProcessingVideo = false;

(async () => {
  try {
    // Get external ip as the identifier of the vm
    let ip = await publicIp.v4({
      fallbackUrls: ["https://ifconfig.co/ip"],
    });
    ip = ip.split(".").join("_");

    logger.info(`Starting Zoom Sensor on ${ip}...`);
    logger.info(`ZoomAppKey: ${process.env.ZoomAppKey}`);
    logger.info(`ZoomAppSecret: ${process.env.ZoomAppSecret}`);
    logger.info(`FirebaseKey: ${process.env.FirebaseKey}`);
    logger.info(`FirebaseAuthEmail: ${process.env.FirebaseAuthEmail}`);
    logger.info(`FirebaseAuthPwd: ${process.env.FirebaseAuthPwd}`);
    logger.info(`FirebaseUrl: ${process.env.FirebaseUrl}`);
    logger.info(`FirebaseBucketUrl: ${process.env.FirebaseBucketUrl}`);
    logger.info(`ZoomsenseUrl: ${process.env.ZoomsenseUrl}`);
    logger.info(
      `DockerImage: ${process.env.DockerImage ||
        "Defaulted to zoomsense/windowssensor:latest"}`
    );
    logger.info(`Capacity: ${process.env.Capacity || "Defaulted to 12"}`);
    logger.info(`ProcessRecordings: ${process.env.ProcessRecordings}`);

    await db
      .ref(`scheduling/${ip}`)
      .update({ capacity: process.env.Capacity || 12 });

    // Listen for children added to the current scheduling node (also fires for existing children in the path on startup)
    schedulingRef
      .child(`${ip}/sensors`)
      .on("child_added", (snapshot) => (scheduleSensor(snapshot, ip)));

    // Listener for child removed for the current scheduling node
    schedulingRef
      .child(`${ip.split(".").join("_")}/sensors`)
      .on("child_removed", async function(snapshot) {
        try {
          const {meetingNo, startTime, sensorId} = getDetailsFromScheduleKey(snapshot.key);
          const containerName = buildDockerContainerName(meetingNo, startTime, sensorId);

          const removeSeconds = 15;

          logger.info(
            `The schedule node for Zoom Sensor ${containerName} was removed, scheduling container removal in ${removeSeconds} seconds.`
          );

          // Stop Zoom Sensor and remove the Docker container to release resources
          schedule.scheduleJob(
            new Date(Date.now() + removeSeconds * 1000),
            function() {
              pushRemoveDockerCon(containerName);
            }
          );
        } catch (err) {
          logger.error(err);
        }
      });

    if (process.env.ProcessRecordings) {
      try {
        fs.mkdirSync(path.join(".", "tmp"), true);
      } catch (err) {
        // Already exists
      }

      // Check for more queue every 30 seconds
      await checkRecordings(ip);
      setInterval(async function() {
        await checkRecordings(ip);
      }, 30 * 1000);
    }

    // Check for crashed sensors every minute
    pushDockerList(ip);
    setInterval(function() {
      pushDockerList(ip);
    }, 60 * 1000);

  } catch (err) {
    logger.error("Error: ", err);
  }
})();

async function scheduleSensor(snapshot, ip) {
  try {
    const newSchedule = snapshot.val();
    const {meetingNo, sensorId} = getDetailsFromScheduleKey(snapshot.key);
    const {uid, password, webinarToken, startTime} = newSchedule;
    // If a sensor updates "lastSeen" after the scheduling node has been deleted, it creates the scheduling node again,
    // and triggers this code.  In that case, the lastSeen field is the only thing populated under the scheduling node.
    // Verify that the values from the snapshot are valid before proceeding with spinning up a sensor.
    if (!uid || !startTime) {
      return;
    }
    const containerName = buildDockerContainerName(meetingNo, moment(startTime), sensorId);
    if (await getContainerByName(containerName)) {
      logger.info(`A container for meeting ${snapshot.key} is already running - doing nothing.`);
      return;
    }
    logger.info(`Scheduling a container for meeting ${snapshot.key}: ${startTime}`);
    const websiteDomain = `${process.env.ZoomsenseUrl}/${meetingNo}`;
    const scheduledStartDate = moment(startTime).toDate();
    if (scheduledStartDate.getTime() > Date.now()) {
      // Schedule Zoom Sensor to start at the specified start time
      schedule.scheduleJob(scheduledStartDate, async function() {
        pushDockerCon(
          meetingNo,
          sensorId,
          websiteDomain,
          password,
          ip,
          uid,
          startTime,
          webinarToken
        );
        snapshot.ref.update({ scheduled: true });
      });
    } else {
      // Start the Zoom Sensor immediately
      pushDockerCon(
        meetingNo,
        sensorId,
        websiteDomain,
        password,
        ip,
        uid,
        startTime,
        webinarToken
      );
      snapshot.ref.update({ scheduled: true });
    }
  } catch (err) {
    logger.error(`Error trying to schedule sensor for meeting ${snapshot.key}:`, err);
  }
}

// Check whether there are transcoding jobs remaining on the processing queue
async function checkRecordings(ip) {
  if (!isProcessingVideo) {
    isProcessingVideo = true;

    let latest = await recordingsRef
      .child(ip)
      .orderByKey()
      .limitToFirst(1)
      .once("value");

    if (latest.numChildren() === 1) {
      const key = Object.keys(latest.val())[0];
      const file = latest.child(key).val();

      // Remove the last bit of the file path and get all files from bucket
      let dir = file.filePath.split("/");
      dir.pop();
      const rawVideoPath = dir.join("/");
      const [files] = await bucket.getFiles({
        prefix: rawVideoPath,
      });

      // Download all files in the following directory
      for (const file of files)
        await file.download({
          destination: path.join(".", "tmp", file.name.split("/").pop()),
        });

      // Run video transcoder container for the .meetingrec files
      const tmpPath = path.join(__dirname, "tmp");
      pushDockerTranscode(tmpPath);

      // Wait until the .mp4 file exists or the container exits
      const finalVideoPath = path.join(__dirname, "tmp/meetingrec_0.mp4");
      const timeout = setInterval(async function() {
        const fileExists = fs.existsSync(finalVideoPath);
        if (fileExists) {
          clearInterval(timeout);

          // Upload to Firebase Bucket
          await bucket.upload(finalVideoPath, {
            destination: rawVideoPath + "/meetingrec_0.mp4",
          });

          // Remove all local files
          let retryCount = 1;
          while (retryCount <= 10) {
            const success = removeTmpFiles(tmpPath);
            if (success) break;
            else {
              await sleep(5000);
              retryCount++;
            }
          }

          await updateTranscodingQueue(rawVideoPath, ip, key);
          isProcessingVideo = false;
        }
      }, 10 * 1000);
    } else isProcessingVideo = false;
  }
}

// Clear local temporary transcoding files
function removeTmpFiles(tmpPath) {
  try {
    const files = fs.readdirSync(tmpPath);
    files.forEach((file) => {
      fs.unlinkSync(path.join(tmpPath, file));
    });
    return true;
  } catch (error) {
    return false;
  }
}

// Update the transcoding queue on Firebase
async function updateTranscodingQueue(rawVideoPath, ip, key) {
  try {
    // Update transcoding information to Firebase
    const filePathArr = rawVideoPath.split("/");
    const meetingId = filePathArr[1];
    const sensorId = filePathArr[2].split("-")[0];
    const timestamp = filePathArr[2].split("-")[1];
    await activeSpeakerRef
      .child(`${meetingId}/${sensorId}/history/${timestamp}/finishTranscode`)
      .set(true);

    // Remove from processing queue
    await recordingsRef
      .child(ip)
      .child(key)
      .remove();
  } catch (error) {
    console.error(error);
  }
}

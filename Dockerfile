FROM mcr.microsoft.com/windows/servercore:1809

ENV NPM_CONFIG_LOGLEVEL info
ENV NODE_VERSION 12.16.3
ENV NODE_SHA256 03450c12d04fc34c2d857a6a2b11ab1cfee7e0631546ab3f25eeb6452e83803f

RUN powershell -Command \
    wget -Uri https://nodejs.org/dist/v%NODE_VERSION%/node-v%NODE_VERSION%-x64.msi -OutFile node.msi -UseBasicParsing ; \
    if ((Get-FileHash node.msi -Algorithm sha256).Hash -ne $env:NODE_SHA256) {exit 1} ; \
    Start-Process -FilePath msiexec -ArgumentList /q, /i, node.msi -Wait ; \
    Remove-Item -Path node.msi

WORKDIR C:/ZoomSense

COPY package.json C:/ZoomSense

RUN npm install --silent

COPY . C:/ZoomSense

CMD [ "node.exe", "index.js" ]
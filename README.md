[![Build status](<https://dev.azure.com/zoomsense/windowssensor/_apis/build/status/windowssensor-Docker%20container-CI%20(1)>)](https://dev.azure.com/zoomsense/windowssensor/_build/latest?definitionId=2)

![Docker Image Version (latest by date)](https://img.shields.io/docker/v/zoomsense/zoomsense-manager?style=plastic)

`docker pull zoomsense/zoomsense-manager`

# ZoomSense - Docker Scheduler

ZoomSense Docker Scheduler is containerised NodeJS application powered by **Firebase Real-time Database** and **Docker API** which performs lower-level schedulings for Docker containers to start Zoom Sensors as requested on each VM within the ZoomSense cloud. The major features of ZoomSense Docker Scheduler including:

- Implement listener for new scheduling requests from the higher-level scheduler
- Scheduler and initialize new containerised ZoomSensors
- Release resources (Docker containers) when meetings finish

The current implementation uses the external IPv4 address of the Cloud VM as the unique identifier for registering listeners for the scheduling node.

# Getting Started (Local Development)

## Prerequisites

- A Local Windows Machine or Windows VM with Docker installed
- [Zoom SDK App](https://gitlab.com/action-lab-aus/zoomsense/zoomsense/-/blob/master/docs/4_QuickStart_Zoom_SDK.md)
- [Firebase Project](https://gitlab.com/action-lab-aus/zoomsense/zoomsense/-/blob/master/docs/2_QuickStart_Firebase.md) with **Blaze Plan (Pay as you go)**

## 1. Download the Firebase Admin Service Account

Under the Firebase console, go to `Settings > Service Accounts`. Under the **Firebase Admin SDK** section, click `Generate new private key` to create a Firebase service account that can be used to authenticate Firebase features programmatically.

## 2. Create an Admin Account in Firebase with Custom Claims

Under the **Build** section in the Firebase console, choose **Authentication** and click **Add user** to start creating the admin user account. Provide the email and password for the account and click **Add user** to finalize the creation.

You will be able to see the **User UID** after the creation.

Via the Firebase Admin SDK, you can set Custom User Claim for the admin user with security rules to control access. For example, the following code can be used to set the admin Custom User Claim in Node.js:

```JS
// Set admin privilege on the user corresponding to uid.
admin.auth().setCustomUserClaims("User_UID", { admin: true }).then(() => {
  // The new custom claims will propagate to the user's ID token the
  // next time a new one is issued
});
```

More details can be found on the [**Custom User Claim**](https://firebase.google.com/docs/auth/admin/custom-claims) section on Firebase.

## 3. Generate **Zoom SDK** Key and Secret

Go to [Zoom MarketPlace](https://marketplace.zoom.us/) and create a Zoom account.

Under the **Develop** tab, select **Build App**. Choose SDK to start creating an SDK App on Zoom. Provide basic information and developer details under the **Information** section including:

- Company Name
- Developer Contact Name
- Developer Contact Email Address

Under the **App Credentials** section, you will be able to find the SDK Key & Secret for developing the ZoomSensor application.

## 4. Configure Environment Variables

Create a `.env` file under the root directly with the following credentials:

```
FirebaseUrl=https://FIREBASE_DATABASE_URL.firebaseio.com
ZoomsenseUrl=https://FIREBASE_PROJECT_ID.web.app/#
ZoomAppKey=Zoom SDK App Key
ZoomAppSecret=Zoom SDK App Secret
FirebaseKey=Firebase App Key
FirebaseAuthEmail=Firebase Admin Account Auth Email
FirebaseAuthPwd=Firebase Admin Account Auth Password
FirebaseBucketUrl=FIREBASE_PROJECT_ID.appspot.com
DockerPort=2375
DockerImage=zoomsense/windowssensor:latest
ProcessRecordings=false
FirebaseAdmin=Firebase Admin Service Account Credentials
```

## 5. Start the Docker Scheduler

To start the Docker Scheduler locally, run:

```
node index.js
```

Please note the first time you start the Docker Manager will take a bit of time to download the docker image.

# Migrate to the Live Environment

To run the ZoomSense Docker Scheduler in the live environment, you can use Windows Cloud VMs to achieve that. More details for configuring the live environment can be found under the [QuickStart Guide](https://gitlab.com/action-lab-aus/zoomsense/zoomsense/-/blob/master/docs/11_QuickStart_VM.md).

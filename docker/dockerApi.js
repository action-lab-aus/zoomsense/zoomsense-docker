const { logger } = require("../logger/logger");
const Docker = require("dockerode");
const { db } = require("../util/admin");
const schedule = require("node-schedule");
const moment = require("moment-timezone");
const { buildDockerContainerName } = require("../util/naming");

const docker = new Docker();
let queue = [];
let queueProcessing = false;

/**
 * Await and get the current Docker information
 */
async function dockerInfo() {
  try {
    return await docker.info();
  } catch (error) {
    console.error(error);
  }
}

/**
 * Push new schedules for create and start Docker containers into the queue
 * @param {*} meetingNo Meeting Id
 * @param {*} sensorId Zoom Sensor Id
 * @param {*} websiteDomain Visualization Url
 * @param {*} password Meeting password (optional)
 * @param {*} ip External Ip address of the vm
 * @param {*} uid Meeting hosts's uid in Firebase
 * @param {*} startTime Start time of the meeting
 * @param {*} webinarToken Webinar token for joining as panelist
 */
function pushDockerCon(
  meetingNo,
  sensorId,
  websiteDomain,
  password,
  ip,
  uid,
  startTime,
  webinarToken
) {
  queue.push({
    meetingNo,
    sensorId,
    websiteDomain,
    password,
    ip,
    uid,
    startTime,
    webinarToken,
    process: "start",
  });

  processQueue();
}

/**
 * Push new schedules for video transcoding into the queue
 * @param {*} path Path of the .meetingrec files to be transcoded
 */
function pushDockerTranscode(path) {
  queue.push({
    path,
    process: "transcode",
  });

  processQueue();
}

/**
 * Push new list container request into the queue
 * @param {*} ip External Ip address of the vm
 */
function pushDockerList(ip) {
  queue.push({
    ip: ip,
    process: "list",
  });

  processQueue();
}

/**
 * Push new docker inspect request into the queue
 * @param {*} container Container to be inspected
 * @param {*} ip External Ip address of the vm
 */
function pushDockerInspect(container, ip) {
  queue.push({
    container: container,
    ip: ip,
    process: "inspect",
  });

  processQueue();
}

/**
 * Push a job to remove the named Docker container into the queue.
 * @param {*} containerName The container name to remove.
 */
function pushRemoveDockerCon(containerName) {
  queue.push({
    containerName,
    process: "stop",
  });

  processQueue();
}

/**
 * Process the queue for jobs related to Docker containers
 */
async function processQueue() {
  if (queueProcessing) return;

  queueProcessing = true;
  while (queue.length) {
    const job = queue.pop();
    try {
      switch (job.process) {
        case "start":
          await runDockerConQueue(
            job.meetingNo,
            job.sensorId,
            job.websiteDomain,
            job.password,
            job.ip,
            job.uid,
            job.startTime,
            job.webinarToken
          );
          break;
        case "transcode":
          await runTranscode(job.path);
          break;
        case "list":
          await runListContainers(job.ip);
          break;
        case "inspect":
          await runDockerInspect(job.container, job.ip);
          break;
        case "stop":
          await removeDockerConQueue(job.containerName);
          break;
        default:
          logger.error(`Unknown job type "${job.process}" - ignoring job ${JSON.stringify(job)}`);
      }
    } catch (e) {
      console.error(e);
    }
  }
  queueProcessing = false;
}

/**
 * Create and start Docker container with the name `MeetingId-SensorId`
 * @param {*} meetingNo Meeting Id
 * @param {*} sensorId Zoom Sensor Id
 * @param {*} websiteDomain Visualization Url
 * @param {*} password Meeting Password
 * @param {*} ip External Ip address of the vm
 * @param {*} uid Meeting hosts's uid in Firebase
 * @param {*} startTime Start time of the meeting
 * @param {*} webinarToken Webinar token for joining as a panelist
 */
async function runDockerConQueue(
  meetingNo,
  sensorId,
  websiteDomain,
  password,
  ip,
  uid,
  startTime,
  webinarToken
) {
  try {
    logger.info(`Meeting No: ${meetingNo}`);
    logger.info(`Sensor Id: ${sensorId}`);
    logger.info(`Website Domain: ${websiteDomain}`);
    logger.info(`Meeting Password: ${password}`);
    logger.info(`Meeting Start Time: ${startTime}`);
    logger.info(`Webinar Token: ${webinarToken}`);

    const containerName = buildDockerContainerName(meetingNo, moment(startTime), sensorId);
    let cmdStr = `C:\\ZoomSensor\\ZoomSensor.exe ${meetingNo} ${sensorId} ${websiteDomain} ${password} ${startTime}`;
    if (webinarToken) cmdStr += ` ${webinarToken}`;

    const container = await docker.createContainer({
      Image: process.env.DockerImage || "zoomsense/windowssensor:latest",
      Tty: false,
      name: containerName,
      AttachStdout: true,
      AttachStderr: true,
      Env: [
        `ZoomAppKey=${process.env.ZoomAppKey}`,
        `ZoomAppSecret=${process.env.ZoomAppSecret}`,
        `FirebaseKey=${process.env.FirebaseKey}`,
        `FirebaseAuthEmail=${process.env.FirebaseAuthEmail}`,
        `FirebaseAuthPwd=${process.env.FirebaseAuthPwd}`,
        `FirebaseUrl=${process.env.FirebaseUrl}`,
        `FirebaseBucketUrl=${process.env.FirebaseBucketUrl}`,
        `ExternalIp=${ip}`,
        `MeetingUid=${uid}`,
      ],
      Cmd: ["powershell", "-c", cmdStr],
    });

    await container.start();
  } catch (err) {
    logger.error("Run Docker Error: ", err);
    // HTTP Code 500 Server Error - CreateComputeSystem
    // When an error occurs, try again in 30 seconds
    schedule.scheduleJob(new Date(Date.now() + 30 * 6000), function() {
      pushDockerCon(
        meetingNo,
        sensorId,
        websiteDomain,
        password,
        ip,
        uid,
        startTime,
        webinarToken
      );
    });
  }
}

/**
 * Create and start transocder container for video transcoding
 * @param {*} path Path of the .meetingrec files to be transcoded
 */
async function runTranscode(path) {
  try {
    const containerName = `transcoder`;
    const transcoderCon = await getContainerByName(containerName);
    if (transcoderCon) await transcoderCon.remove({ force: true });

    const options = {
      Image: process.env.DockerImage || "zoomsense/windowssensor:latest",
      Tty: false,
      name: containerName,
      AttachStdout: true,
      AttachStderr: true,
      HostConfig: {
        CPUPercent: 20,
        Mounts: [
          {
            Target: "C:\\Recordings",
            Source: "documents_rec-transcoding",
            Type: "volume",
            ReadOnly: false,
          },
        ],
      },
      Cmd: [
        "powershell",
        "-c",
        "Start-Job -Name Transcode -ScriptBlock {C:\\ZoomSensor\\zTscoder.exe C:\\Recordings}; Start-Sleep -s 36000",
      ],
    };

    const container = await docker.createContainer(options);
    await container.start();
  } catch (err) {
    logger.error("Run Docker Transcode Error: ", err);
  }
}

/**
 * Remove the Docker container
 * @param {*} containerName Container name which runs the current Zoom Sensor
 */
async function removeDockerConQueue(containerName) {
  try {
    const container = await getContainerByName(containerName);
    if (container) await container.remove({ force: true });
  } catch (err) {
    logger.error("Remove Docker Container Error: ", err);
  }
}

/**
 * List all docker containers
 * @param {*} ip External Ip address of the vm
 */
async function runListContainers(ip) {
  try {
    const containers = await docker.listContainers({ all: true });
    containers.forEach(function(containerInfo) {
      const container = docker.getContainer(containerInfo.Id);
      pushDockerInspect(container, ip);
    });
  } catch (err) {
    logger.error("List Docker Containers Error: ", err);
  }
}

/**
 * Inspect the docker container to check the exit status code. If it is non-zero, restart
 * the docker container to join the meeting
 * @param {*} container Container to be inspected
 * @param {*} ip External Ip address of the vm
 */
async function runDockerInspect(container, ip) {
  try {
    const data = await container.inspect();
    const exitCode = data.State.ExitCode;
    const imageName = data.Config.Image;
    const containerName = data.Name;

    if (
      exitCode !== 0 &&
      imageName.startsWith("zoomsense/windowssensor") &&
      containerName !== "/transcoder"
    ) {
      logger.error("Sensor container exited unexpectedly.");
      logger.error(`\tExit Status: ${exitCode}`);
      logger.error(`\tImage Name: ${imageName}`);
      logger.error(`\tContainer Name: ${containerName}`);

      try {
        const logBuffer = await container.logs({ stdout: true, stderr: true, timestamps: true });
        console.log(`===== Start docker logs for ${containerName} =====`);
        console.log(logBuffer.toString());
        console.log(`====== End docker logs for ${containerName} ======`);
      } catch (e) {
        logger.error(`\tFailed to capture logs from container: ${e}`);
      }

      // Remove the current exited container
      await removeDockerConQueue(containerName);

      // Get some details from the container name (which starts with a leading /, hence `substring(1)`)
      const [meetingNo, startTime, sensorId] = containerName.substring(1).split("-");

      const meetingId = `${meetingNo}_${startTime}`;
      const websiteDomain = `${process.env.ZoomsenseUrl}/${meetingId}`;

      // Retrieve the meetings from Firebase
      const snapshot = await db.ref("meetings").once("value");
      const allMeetings = snapshot.val();

      // Loop through the meetings and find the meeting details for the crashed sensor
      for (const userId in allMeetings) {
        const matchingMeeting = allMeetings[userId][meetingId];
        if (matchingMeeting) {
          logger.info(`Restarting ZoomSensor container: ${containerName}`);

          // Push the new docker run request onto the queue for restarting
          pushDockerCon(
            meetingNo,
            sensorId,
            websiteDomain,
            matchingMeeting.password,
            ip,
            userId,
            startTime,
            matchingMeeting.webinarToken
          );

          return;
        }
      }

      logger.error(`Failed to find meeting "${meetingId}" for crashed ZoomSensor container ${containerName} - container not restarted!`);
    }
  } catch (err) {
    logger.error("Inspect Docker Container Error: ", err);
  }
}

/**
 * Get the Docker container by name (`MeetingId-SensorId`)
 * @param {*} conName Name of the container
 */
async function getContainerByName(conName) {
  try {
    // List all containers with the name specified
    const containers = await docker.listContainers({
      limit: 1,
      filters: `{"name": ["${conName}"]}`,
    });

    // Return the container with the corresponding Id
    if (containers.length !== 0) return docker.getContainer(containers[0].Id);
  } catch (err) {
    logger.error("Get Docker Container Error: ", err);
  }
  return null;
}

exports.dockerInfo = dockerInfo;
exports.pushDockerList = pushDockerList;
exports.pushDockerCon = pushDockerCon;
exports.pushDockerTranscode = pushDockerTranscode;
exports.pushRemoveDockerCon = pushRemoveDockerCon;
exports.getContainerByName = getContainerByName;

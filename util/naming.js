const moment = require("moment-timezone");

exports.timeAsKey = (date) => (
  moment(date).utc().format("YYYYMMDDTHHmmss")
);

exports.buildDockerContainerName = (meetingNo, startTime, sensorId) => (
  typeof(startTime) === "string"
    ? `${meetingNo}-${startTime}-${sensorId}`
    : `${meetingNo}-${exports.timeAsKey(startTime)}-${sensorId}`
)

exports.getDetailsFromScheduleKey = (scheduleKey) => {
    // Parse the meeting details.  scheduleKey is in the format (numeric meeting id)_(start time)-(sensor index)
    const [fullMeetingId, sensorIndex] = scheduleKey.split("-");
    const [meetingNo, startTime] = fullMeetingId.split("_");
    const sensorId = `ZoomSensor_${sensorIndex}`;
    return {meetingNo, startTime, sensorId};
}
const admin = require("firebase-admin");
// If the env value has actual newlines in it, convert them back to "\n" before trying to parse as JSON.
let firebaseAdminJson = process.env.FirebaseAdmin.replace(/\n/g, "\\n");
const serviceAccount = JSON.parse(firebaseAdminJson);

// Initialize Firebase application
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: process.env.FirebaseUrl.trim(),
  storageBucket: process.env.FirebaseBucketUrl.trim(),
});

const db = admin.database();
const schedulingRef = db.ref("scheduling");
const activeSpeakerRef = db.ref("data/activeSpeakers");
const recordingsRef = db.ref("processing/recordings");
const bucket = admin.storage().bucket();

exports.schedulingRef = schedulingRef;
exports.activeSpeakerRef = activeSpeakerRef;
exports.recordingsRef = recordingsRef;
exports.db = db;
exports.bucket = bucket;

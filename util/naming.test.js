const chai = require("chai");
const moment = require('moment-timezone');
const { timeAsKey, buildDockerContainerName } = require("./naming");

describe("naming functions", () => {

  describe("timeAsKey function", () => {

    it("should format a date as expected", () => {
      const startTime = new Date("2021-03-08T04:00:00.000Z");
      const asKey = timeAsKey(startTime);
      chai.assert.equal(asKey, "20210308T040000");
    });

  });

  describe("buildDockerContainerName function", () => {

    it("should take startTime as a moment object and produce the expected container name", () => {
      const containerName = buildDockerContainerName(81886228923, moment("2022-05-11T00:30:00.000Z"), 'ZoomSensor_1');
      chai.assert.equal(containerName, "81886228923-20220511T003000-ZoomSensor_1");
    });

    it("should take startTime as a date and produce the expected container name", () => {
      const containerName = buildDockerContainerName(81886228923, new Date("2022-05-11T00:30:00.000Z"), 'ZoomSensor_1');
      chai.assert.equal(containerName, "81886228923-20220511T003000-ZoomSensor_1");
    });

    it("should take startTime as a string and produce the expected container name", () => {
      const containerName = buildDockerContainerName(81886228923, "20220511T003000", 'ZoomSensor_1');
      chai.assert.equal(containerName, "81886228923-20220511T003000-ZoomSensor_1");
    });

  });


});
const { createLogger, format, transports } = require("winston");
const { combine, timestamp, label, printf } = format;

const logFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});

// Create winston logger and add transports to log files
const logger = createLogger({
  level: "info",
  format: combine(label({ label: "Zoom Sensor" }), timestamp(), logFormat),

  transports: [
    new transports.Console({
      level: "debug",
    }),
  ],
});

exports.logger = logger;
